# Team F - Operations Research Final Project
## Easrfd - Explatory Analysis of Software Reliability Failure DATA

### Team members
* Barry Harris
* Brittany
* Syd
* Ekene

### Getting Started
After cloning the repo, install all needed dependencies

``` bash
git clone https://bitbucket.org/Shadeslayer345/easrfd

cd easrfd

npm install

npm run jspm:install
```

Next you'll need to build the server and run it in dev mode

``` bash
npm run start:dev
```

Then open up your browser to localhost:3000 and load in a file!
Files are located in the directory called test_data

### Making new charts
To make a new chart, copy the scatter-plot.js file and change accordingly.

c3 expects x coordinates to be loaded as multiple arrays
c3 expects y coordinates to be loaded as a single array

import ip from 'ip';

const
  error = (err, req, res, next) => {
    res.status(500);
    res.render(`500.jade`, {error: err, title: `500: Internal Server Error`});
  },
  index = (req, res) => {
    (process.env.NODE_ENV === `production`) ?
        res.render(`index`, {prod: true}) :
        res.render(`index`, {prod: false, addr: ip.address()});
  },
  notFound = (req, res) => {
    res.status(404);
    res.render(`404.jade`, {title: `404: Resource not found`});
  };

export default {
  error,
  index,
  notFound
};

import c3 from 'c3';

const load = (inputData) => {
  const dataSet = {
    x: [`Number of Failures`],
    y: [`y`]
  };
  let total = 0;

  for (let i = 0; i < inputData.length; i++) {
    total += inputData[i];
    dataSet.x.push(total);
    dataSet.y.push(inputData[i]);
  }

  c3.generate({
    bindto: `#graph`,
    data: {
      x: `Number of Failures`,
      columns: [
        dataSet.x,
        dataSet.y
      ]
    },
    axis: {
      x: {
        label: {
          text: `Number of Failures`,
          position: `outer-left`
        },
        tick: {
          fit: false
        }
      },
      y: {
        label: `Interfailure Time`,
        position: `outer-top`
      }
    },
    legend: {
      position: `inset`,
      inset: {
        anchor: `top-right`
      }
    },
    tooltip: {
      format: {
        name: () => {
          return `Time of interfailure`;
        },
        title: () => {
          return `Data`;
        },
        value: (val) => {
          const format = d3.format(`,`);

          return `${format(val)} sec`;
        }
      }
    }
  });

  console.log(`Time Series Loaded!`);
};

export default {
  load
};

import c3 from 'c3';

import utils from '../utils/utils.js';

const load = (inputData) => {
  const dataSet = {
    sums: [],
    x: [`Number of Failures`],
    y: [`y`]
  };
  let sum = 0;

  for (let i = 0, n = inputData.length; i < n; i++) {
    const
      coef = 1 / n,
      tO = inputData[i];
    let sI = 0;

    sum += inputData[i];
    dataSet.sums.push(sum);
    sI = dataSet.sums.reduce(utils.sumAll);
    dataSet.x.push(i);
    dataSet.y.push((coef * (sI - (tO / 2))) / (tO * Math.sqrt(1 / (12 * n))));
  }

  c3.generate({
    bindto: `#graph`,
    data: {
      x: `Number of Failures`,
      columns: [
        dataSet.x,
        dataSet.y
      ]
    },
    axis: {
      x: {
        label: {
          text: `Number of Failures`,
          position: `outer-left`
        },
        tick: {
          fit: false
        }
      },
      y: {
        label: {
          text: `LaPlace Value`,
          position: `outer-top`
        }
      }
    },
    legend: {
      position: `inset`,
      inset: {
        anchor: `top-left`
      }
    },
    tooltip: {
      format: {
        name: () => {
          return `LaPlace Value`;
        },
        title: () => {
          return `Data`;
        },
        value: (val) => {
          const format = d3.format(`,`);

          return `${format(val)} sec`;
        }
      }
    }
  });
  console.log(`LaPlace loaded!`);
};

export default {
  load
};

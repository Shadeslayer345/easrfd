import c3 from 'c3';

const load = (inputData, dp = 1) => {
  const
    dataSet = {
      x: [`% Test Time`],
      y: [`y`]
    },
    total = inputData.reduce((p, c) => {
      return p + c;
    }, 0);

  for (let i = 0, j = 0; i < inputData.slice(0, inputData.length * dp).length; i++) {
    j += inputData[i];
    dataSet.x.push((j / total));
    dataSet.y.push(i);
  }

  c3.generate({
    bindto: `#graph`,
    data: {
      x: `% Test Time`,
      columns: [
        dataSet.x,
        dataSet.y
      ]
    },
    axis: {
      x: {
        label: {
          text: `% Test Time`,
          position: `outer-left`
        },
        tick: {
          fit: false
        }
      },
      y: {
        label: {
          text: `Cumulative Quantity of Failures`,
          position: `outer-top`
        },
        tick: {
          fit: false
        }
      }
    },
    legend: {
      position: `inset`,
      inset: {
        anchor: `top-left`
      }
    },
    tooltip: {
      format: {
        name: () => {
          return `# of Failures up to this point`;
        },
        title: () => {
          return `Data`;
        },
        value: (val) => {
          const format = d3.format(`,`);

          return `${format(val)}`;
        }
      }
    }
  });
  console.log(`Cumulative Plot Loaded!`);
};

export default {
  load
};

import c3 from 'c3';

const load = (inputData, full = true) => {
  const
    histogramCategories = (full) ? [
      `0-10`, `10-20`, `20-30`, `30-40`, `40-50`,
      `50-60`, `60-70`, `70-80`, `80-90`, `90-100`
    ] : [
      `0-3`, `3-6`, `6-9`, `9-12`, `12-15`,
      `15-18`, `18-21`, `21-24`, `24-27`, `27-30`
    ],
    dataSet = {
      y: [`y`, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    },
    total = inputData.reduce((p, c) => {
      return p + c;
    }, 0);

  if (full) {
    for (let i = 0, j = 0; i < inputData.length; i++) {
      j += inputData[i];

      if ((j / total) * 100 <= 10) {
        dataSet.y[1]++;
      } else if ((j / total) * 100 <= 20) {
        dataSet.y[2]++;
      } else if ((j / total) * 100 <= 30) {
        dataSet.y[3]++;
      } else if ((j / total) * 100 <= 40) {
        dataSet.y[4]++;
      } else if ((j / total) * 100 <= 50) {
        dataSet.y[5]++;
      } else if ((j / total) * 100 <= 60) {
        dataSet.y[6]++;
      } else if ((j / total) * 100 <= 70) {
        dataSet.y[7]++;
      } else if ((j / total) * 100 <= 80) {
        dataSet.y[8]++;
      } else if ((j / total) * 100 <= 90) {
        dataSet.y[9]++;
      } else if ((j / total) * 100 <= 100) {
        dataSet.y[10]++;
      }
    }
  } else {
    for (let i = 0, j = 0; i < inputData.length; i++) {
      j += inputData[i];

      if ((j / total) * 100 <= 3) {
        dataSet.y[1]++;
      } else if ((j / total) * 100 <= 6) {
        dataSet.y[2]++;
      } else if ((j / total) * 100 <= 9) {
        dataSet.y[3]++;
      } else if ((j / total) * 100 <= 12) {
        dataSet.y[4]++;
      } else if ((j / total) * 100 <= 15) {
        dataSet.y[5]++;
      } else if ((j / total) * 100 <= 18) {
        dataSet.y[6]++;
      } else if ((j / total) * 100 <= 21) {
        dataSet.y[7]++;
      } else if ((j / total) * 100 <= 24) {
        dataSet.y[8]++;
      } else if ((j / total) * 100 <= 27) {
        dataSet.y[9]++;
      } else if ((j / total) * 100 <= 30) {
        dataSet.y[10]++;
      }
    }
  }

  c3.generate({
    bindto: `#graph`,
    data: {
      columns: [
        dataSet.y
      ],
      type: `bar`
    },
    axis: {
      x: {
        type: `category`,
        categories: histogramCategories,
        label: {
          text: `Number of Failures`,
          position: `outer-left`
        }
      },
      y: {
        label: {
          text: `% of Test Time`,
          position: `outer-top`
        },
        tick: {
          fit: false
        }
      }
    },
    legend: {
      position: `inset`,
      inset: {
        anchor: `top-left`
      }
    },
    tooltip: {
      format: {
        name: () => {
          return `# of Failures`;
        },
        title: () => {
          return `Data`;
        },
        value: (val) => {
          return `${val}`;
        }
      }
    }
  });

  console.log(`Histogram Loaded!`);
};

export default {
  load
};

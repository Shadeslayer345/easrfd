import c3 from 'c3';

const load = (inputData) => {
  const dataSet = {
    x: [`Number of Failures`],
    y: [`y`]
  };
  let sum = 0;

  for (let i = 1; i < inputData.length; i++) {
    sum += inputData[i]
    dataSet.x.push(i);
    dataSet.y.push((1 / i) * (sum));
  }

  c3.generate({
    bindto: `#graph`,
    data: {
      x: `Number of Failures`,
      columns: [
        dataSet.x,
        dataSet.y
      ]
    },
    axis: {
      x: {
        label: {
          text: `Number of Failures`,
          position: `outer-left`
        },
        tick: {
          fit: false
        }
      },
      y: {
        label: {
          text: `Arithmetic Value`,
          position: `outer-top`
        },
        tick: {
          fit: false
        }
      }
    },
    legend: {
      position: `inset`,
      inset: {
        anchor: `top-left`
      }
    },
    tooltip: {
      format: {
        name: () => {
          return `Arithmetic Value`;
        },
        title: () => {
          return `Data`;
        },
        value: (val) => {
          return `${val} sec`;
        }
      }
    }
  });

  console.log(`Arithmetic loaded!`);
};

export default {
  load
};

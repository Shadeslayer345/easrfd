import c3 from 'c3';

const load = (inputData) => {
  const
    dataSet = {
      x: [`% Test Time`],
      y: [`y`]
    };
  let total = 0;

  for (let i = 0; i < inputData.length; i++) {
    total += inputData[i];
    dataSet.x.push(total / 10000000);
    dataSet.y.push(inputData[i]);
  }

  c3.generate({
    bindto: `#graph`,
    data: {
      x: `% Test Time`,
      columns: [
        dataSet.x,
        dataSet.y
      ],
      type: `scatter`
    },
    axis: {
      x: {
        label: {
          text: `% Test Time`,
          position: `outer-left`
        },
        tick: {
          fit: false
        }
      },
      y: {
        label: {
          text: `Interfailure Times (sec)`,
          position: `outer-top`
        }
      }
    },
    legend: {
      position: `inset`,
      inset: {
        anchor: `top-right`
      }
    },
    tooltip: {
      format: {
        name: () => {
          return `Time since last failure`;
        },
        title: () => {
          return `Data`;
        },
        value: (val) => {
          const format = d3.format(`,`);

          return `${format(val)} sec`;
        }
      }
    }
  });
  console.log(`Scatter Plot Loaded!`);
};

export default {
  load
};

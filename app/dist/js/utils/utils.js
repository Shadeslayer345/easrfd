const
  serialize = (x) => {
    return x.replace(/(\r\n|\n|\r)/gm, ``).split(`.`).map((i) => {
      return parseInt(i.trim(), 10);
    }).filter((y) => {
      return !(isNaN(y));
    });
  },

  sumAll = (p, c) => {
    return p + c;
  };

export default {
  serialize,
  sumAll
};

import arithmeticPlot from './c3-plots/arithmetic-plot.js';
import cumulativePlot from './c3-plots/cumulative-trend-plot.js';
import histogramPlot from './c3-plots/histogram-plot.js';
import laplacePlot from './c3-plots/laplace-trend-plot.js';
import scatterPlot from './c3-plots/scatter-plot.js';
import timeseriesPlot from './c3-plots/time-series-plot.js';
import utils from './utils/utils.js';

if (window.File && window.FileReader && window.FileList && window.Blob) {
  document.querySelector(`#upload`).onchange = (e) => {
    const
      file = e.target.files[0],
      reader = new FileReader();

    reader.onload = () => {
      const inputData = utils.serialize(reader.result);

      document.querySelector(`#scatter`).onclick = () => {
        scatterPlot.load(inputData);
      };
      document.querySelector(`#trends`).onclick = (evt) => {
        const trend = evt.target.value;

        if (trend === `cumulative`) {
          cumulativePlot.load(inputData);
        } else if (trend === `cumulative30`) {
          cumulativePlot.load(inputData, 0.3);
        } else if (trend === `histogram`) {
          histogramPlot.load(inputData);
        } else {
          histogramPlot.load(inputData, false);
        }
      };
      document.querySelector(`#arithmetic`).onclick = () => {
        arithmeticPlot.load(inputData);
      };
      document.querySelector(`#timeseries`).onclick = () => {
        timeseriesPlot.load(inputData);
      };
      document.querySelector(`#laplace`).onclick = () => {
        laplacePlot.load(inputData);
      };
    };

    reader.readAsText(file);
  };
  console.log(`Main loaded!`);
} else {
  console.log(`The File APIs are not fully supported in this browser.`);
}
